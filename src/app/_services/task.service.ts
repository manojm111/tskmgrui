import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';
@Injectable({
  providedIn: 'root'
})
export class TaskService {
  baseURL=environment.BASE_APU_URL+'Task';
  constructor(private http: HttpClient) { }

  // addTask(task:any)
  // {
  //   let header=new HttpHeaders();
  //   header.append('Content-Type','applications/json');
  //   return this.http.post(this.baseURL+'/'+"Task",task,{headers:header});   
  // }

 
  getParentTaskList(){
    let header = new HttpHeaders();
    header.append('Content-Type', 'applications/json');
    return this.http.get(this.baseURL +'/'+ "GetAllParentTask", { headers: header})
}
  addTask(taskName, parentTaskId, priority,startDate,endDate,projectId,userId) {
    
    const objTask = {
      taskName: taskName,
      parentId: parentTaskId,
      priority: priority,
      startDate:startDate,
      endDate:endDate,
      projectId:projectId,
      userId:userId

    }

    return this.http.post(this.baseURL+'/'+"Task", objTask)
     .subscribe(res => console.log('Done'));    

  }

  updateTask(taskName, parentTaskId, priority,startDate,endDate,id) {

    const objTask = {
      taskName: taskName,
      parentId: parentTaskId,
      priority: priority,
      startDate:startDate,
      endDate:endDate

    }
   
    

    let header = new HttpHeaders();
    
    header.append('Access-Control-Allow-Headers', 'Content-Type');
    header.append('Access-Control-Allow-Methods', 'GET');
    header.append('Access-Control-Allow-Origin', '*');      
    
    return this.http.post(this.baseURL+'/UpdateTask/' +id,objTask,{ headers: header})
    .subscribe(res => console.log('Done'));  
  }

  getTask() {    
      return this.http.get(this.baseURL);       
    }

    getTaskById(id:number)
   {
        return this.http.get<number>(this.baseURL+'/'+id);       
    } 
    
   
    deleteTask(id:number):Observable<number>
    {
       return this.http.delete<number>(this.baseURL+'/'+id);
    }

}
