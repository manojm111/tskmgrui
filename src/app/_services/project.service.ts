import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  baseURL=environment.BASE_APU_URL+'project';
  constructor(private http: HttpClient) { }

  addProject(projectName,priority,startDate,endDate,managerId) 
  {
    
    const objProject = {
      projectName: projectName,      
      priority: priority,
      startDate:startDate,
      endDate:endDate,
      userId:managerId,
      Status:true

    }

    return this.http.post(this.baseURL+'/'+"Project", objProject);    

  }


  updateProject(projectName,  priority,startDate,endDate,id) 
  {

    const objProject = {
      projectName: projectName,      
      priority: priority,
      startDate:startDate,
      endDate:endDate

          } 
    

    let header = new HttpHeaders();
    
    header.append('Access-Control-Allow-Headers', 'Content-Type');
    header.append('Access-Control-Allow-Methods', 'GET');
    header.append('Access-Control-Allow-Origin', '*');      
    
    return this.http.post(this.baseURL+'/UpdateProject/' +id,objProject,{ headers: header})
    .subscribe(res => console.log('Done'));  
  }

  getProject() {    
    return this.http.get(this.baseURL);       
  }

  getProjectById(id:number)
 {
      return this.http.get<number>(this.baseURL+'/'+id);       
  } 
 


  suspendProject(id) 
  {
 console.log(id);
    const objProject = {} 
    

    let header = new HttpHeaders();
    
    header.append('Access-Control-Allow-Headers', 'Content-Type');
    header.append('Access-Control-Allow-Methods', 'GET');
    header.append('Access-Control-Allow-Origin', '*');      
    
    return this.http.post(this.baseURL+'/SuspendProject/' +id,objProject,{ headers: header});  
  }

}
