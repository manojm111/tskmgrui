import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class UserService 
{
  baseURL=environment.BASE_APU_URL+'user';
  constructor(private http: HttpClient) { }

  addUser(firstName,lastName,employeeId) 
  {
    
    const objUser = {
      firstName: firstName,      
      lastName: lastName,
      employeeId:employeeId

    }

    return this.http.post(this.baseURL+'/'+"user", objUser);    

  }


  updateUser(firstName,lastName,employeeId,id) 
  {

    const objUser = {
      firstName: firstName,      
      lastName: lastName,
      employeeId:employeeId

          } 
    

    let header = new HttpHeaders();
    
    header.append('Access-Control-Allow-Headers', 'Content-Type');
    header.append('Access-Control-Allow-Methods', 'GET');
    header.append('Access-Control-Allow-Origin', '*');      
    
    return this.http.post(this.baseURL+'/UpdateUser/' +id,objUser,{ headers: header});
    
  }

  getUser() {    
    return this.http.get(this.baseURL);       
  }

  getUserById(id:number)
 {
      return this.http.get<number>(this.baseURL+'/'+id);       
  } 
  deleteUser(id:number):Observable<number>
  {
     return this.http.delete<number>(this.baseURL+'/'+id);
  }

}
