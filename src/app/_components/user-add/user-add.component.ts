import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/_services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import UserModel from 'src/app/_models/user.model';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
  angForm: FormGroup;
  // parentTask: any = [];
  private isAddButtonVisible = true;
  private isUpdateButtonVisible = false;
  users: UserModel[];
  editUser: any = {};
  //selectedParentTaskID:number;
  records: Array<any>;
  isDesc: boolean = false;
  column: string = 'firstName';

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
    this.getAllUser();

  }
  createForm() {
    this.angForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      employeeId: ['', Validators.required]
    });
  }


  //  sort(property){
  //    console.log(pro)
  //   this.isDesc = !this.isDesc; //change the direction    
  //   this.column = property;
  //   let direction = this.isDesc ? 1 : -1;

  //   this.records.sort(function(a, b){

  //       if(a[property] < b[property]){
  //           return -1 * direction;
  //       }
  //       else if( a[property] > b[property]){
  //           return 1 * direction;
  //       }
  //       else{
  //           return 0;
  //       }
  //   });
  // };
  addUser(firstName, lastName, employeeId) {

    this.userService.addUser(firstName, lastName, employeeId)
      .subscribe(res => {        
        console.log("Add user completed")
        this.getAllUser();
      });

  }

  updateUser(firstName, lastName, employeeId) {

    this.userService.updateUser(firstName, lastName, employeeId,this.editUser.UserId)
      .subscribe(res => {        
        console.log("Update user completed")
        this.getAllUser();
      });

    // this.userService.updateUser(firstName, lastName, employeeId, this.editUser.UserId);
    // this.getAllUser();
  }


  getAllUser() {
    this.userService.getUser()
      .subscribe((data: UserModel[]) => {
        console.log(data);
        this.users = data;
        this.records = data;
      });

  }
  deleteUser(id: number) {
    console.log(id);
    this.userService.deleteUser(id).subscribe((id: number) => {
      this.getAllUser();
    });
  }

  EditUser(id: number) {
    console.log(id);
    this.isAddButtonVisible = false;
    this.isUpdateButtonVisible = true;


    this.userService.getUserById(id).subscribe(res => {
      this.editUser = res;
      console.log(this.editUser);
    });

  }
}
