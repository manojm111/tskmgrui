import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/_services/project.service';
import { UserService } from 'src/app/_services/user.service';
import { Router } from '@angular/router';
import ProjectModel from 'src/app/_models/project.model';
//import TaskModel from 'src/app/_models/task.model';

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.css']
})
export class ProjectAddComponent implements OnInit {
  projects: ProjectModel[];
  public searchString: string;
  angForm: FormGroup;
  usersList: any = [];
  editProject: any = {};
  //selectedManagerId: number;
  constructor(private fb: FormBuilder, private projectService: ProjectService, private userService: UserService,private router: Router) {
    this.createForm();
  }

  createForm() {
    console.log(new Date());
    
    this.angForm = this.fb.group({
      projectName: ['', Validators.required],
      priority: [0],
      startDate: [new Date()],
      endDate: [new Date() ],
      ddlUserList:[null, Validators.required],
      chkDate: [false]
    });
  }

//   public onChange(event):void
// {
//   this.selectedManagerId=event.target.value; 

// }

  addProject(projectName, priority, startDate, endDate,managerId) {
    //console.log(manager);
    //console.log(this.selectedManagerId);
    this.projectService.addProject(projectName, priority, startDate, endDate,managerId)
    .subscribe(
      res =>  this.getAllProject()
     )    
    
    
  }

  isDisabled(link)
  {
    console.log(link)

   return link;
  }
  getAllProject()
  { 
    this.projectService.getProject()
    .subscribe((data: ProjectModel[]) => {
      this.projects = data;
    });
  }
  getAllUser()
  {
      this.userService.getUser().subscribe(data => 
        {     
        this.usersList= data;
        console.log(this.usersList);        
      }, error =>
      {
          console.log('Error while gettig user data.');
          });
    }
    
    suspendProject(id: number) 
  { 
    console.log(id);
     this.projectService.suspendProject(id).subscribe(
       res =>  this.getAllProject()
      )    
  }

  EditProject(id: number) {
    console.log(id);

    this.projectService.getProjectById(id).subscribe(res => {
      this.editProject = res;  
      console.log(this.editProject);    
    });
  }

  ngOnInit() {
    
    this.getAllProject();
    this.getAllUser();
    console.log(this.angForm);
  }

}
