import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { TaskService } from 'src/app/_services/task.service';
import {  Router } from '@angular/router';
import TaskModel from 'src/app/_models/task.model';
import ProjectModel from 'src/app/_models/project.model';
import { ProjectService } from 'src/app/_services/project.service';
import { UserService } from 'src/app/_services/user.service';
@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.css']
})
export class TaskAddComponent implements OnInit {
  angForm: FormGroup;
  parentTask: any = [];
  tasks: TaskModel[];
  projectList: ProjectModel[];
  usersList:any=[];
   selectedParentTaskID:number;
   selectedProjectID:number;
  constructor(private fb: FormBuilder, private taskService: TaskService,
    private projectService: ProjectService, private userService:UserService,  private router: Router)
   {
    this.createForm();

 

    }
public onChange(event):void
{
  this.selectedParentTaskID=event.target.value; 

}

getAllParentTask()
{
  this.taskService.getParentTaskList().subscribe(data => 
    {
    //Object.assign(this.parentTask, data);
    this.parentTask= data;    
  }, error => { console.log('Error while gettig category data.'); });


}

getAllProject()
{ 
  this.projectService.getProject()
  .subscribe((data: ProjectModel[]) => {
    this.projectList = data;

    console.log(data);
  });

}

getAllUser()
  {
      this.userService.getUser().subscribe(data => 
        {     
        this.usersList= data;            
      }, error =>
      {
          console.log('Error while gettig user data.');
          });
    }

    createForm()
     {
        this.angForm = this.fb.group({
        taskName: ['', Validators.required ],
        parentTaskId: ['' ],
        priority: [0],
        startDate: ['' ],
        endDate: ['' ],
        chkParentTask: [false],
        ddlProject: [null, Validators.required ],
        ddlUser: [null, Validators.required ]
      });
    }
    addTask(taskName, parentTaskId, priority,startDate,endDate,projectId,userId) 
    {
     console.log(priority);
      this.taskService.addTask(taskName, this.selectedParentTaskID, priority,startDate,endDate,projectId,userId);
      // this.RefreshData();
      // this.router.navigate(['business']);
      }
    
  ngOnInit() {
    // bind drop down on load
  
    this.getAllProject();
    this.getAllParentTask();
    this.getAllUser();
  }
  GetTaskByProjectId()
  {    

    this.taskService.getTaskByProjectId(this.selectedProjectID)
    .subscribe((data: TaskModel[]) => {
      this.tasks = data;
    });
  }

  RefreshData()
  { 
    this.taskService.getTask()
    .subscribe((data: TaskModel[]) => {
      this.tasks = data;
    });
  }

}
