import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { TaskService } from 'src/app/_services/task.service';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

  tasks: any = {};
  angForm: FormGroup;
  parentTask: any = [];
  selectedParentTaskID:number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService,
    private fb: FormBuilder)
     {
      this.createForm();
      
      // bind drop down on load
    this.taskService.getParentTaskList().subscribe(data => 
      {
      //Object.assign(this.parentTask, data);
      this.parentTask= data;    
    }, error => { console.log('Error while gettig parent data.'); });

      }

      createForm() {
        this.angForm = this.fb.group({
        taskName: ['', Validators.required ],
        parentTaskId: ['', Validators.required ],
        priority: ['', Validators.required ],
        startDate: ['', Validators.required ],
        endDate: ['', Validators.required ]
      });
    }

    public onChange(event):void
{
  
  this.selectedParentTaskID=event.target.value;
}

  ngOnInit()
   {
    this.route.params.subscribe(params =>
       {
        this.taskService.getTaskById(params['id']).subscribe(res => {          
        this.tasks = res;            
      console.log(this.tasks)   ;
      });   
   });
  } 
  
  updateTask(taskName, parentTaskId, priority,startDate,endDate)
   {     
      this.route.params.subscribe(params => {
       this.taskService.updateTask(taskName, this.selectedParentTaskID, priority,startDate,endDate, params['id']);
       this.router.navigate(['business']);
 });
}
}
