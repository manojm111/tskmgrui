import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/_services/task.service';
import { ProjectService } from 'src/app/_services/project.service';
import TaskModel from 'src/app/_models/task.model';
import ProjectModel from 'src/app/_models/project.model';

@Component({
  selector: 'app-task-get',
  templateUrl: './task-get.component.html',
  styleUrls: ['./task-get.component.css']
})
export class TaskGetComponent implements OnInit {
  tasks: any[];
  projects: ProjectModel[];
  public searchString: string;
  
  constructor(private taskService: TaskService,private projectService:ProjectService) { }

  ngOnInit() {
    this.getAllProject();
    this.getAllTask();

  }
  
  getAllTask()
  { 
    this.taskService.getTask()
    .subscribe((data: TaskModel[]) => {
      this.tasks = data;
    });

  }
  public onChange(event):void
{
  
  this.getTaskByProjectId(event.target.value)
}
getAllProject()
  { 
    this.projectService.getProject()
    .subscribe((data: ProjectModel[]) => {
      this.projects = data;
    });
  }
  getTaskByProjectId(selectedProjectID:number)
  { 
    //this.tasks = this.projects.filter(t=>t.projectId ==selectedProjectID);    
  }
  deleteTask(id: number) 
  {   

     this.taskService.deleteTask(id).subscribe((id:number)=>
     {
      this.getAllTask();
     });   
  }

}
