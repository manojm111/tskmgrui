import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskAddComponent } from './_components/task-add/task-add.component';
import { TaskGetComponent } from './_components/task-get/task-get.component';
import { TaskEditComponent } from './_components/task-edit/task-edit.component';
import { TaskService } from './_services/task.service';
import { FilterPipe } from './_components/search.pipe';
import { OrderByPipe } from './_components/sort.pipe';

import { ProjectAddComponent } from './_components/project-add/project-add.component';
import { UserAddComponent } from './_components/user-add/user-add.component';



const routes: Routes = [
  {
    path: 'business/create',
    component: TaskAddComponent
  },
  {
    path: 'edit/:id',
    component: TaskEditComponent
  },
  {
    path: 'business',
    component: TaskGetComponent
  },

  {
    path: 'project/create',
    component: ProjectAddComponent
  },
  
  {
    path: 'user/create',
    component: UserAddComponent
  },
  {
    path: 'user/edit/:id',
    component: UserAddComponent
  } 

];

@NgModule({
  declarations: [
    AppComponent,
    TaskAddComponent,
    TaskGetComponent,
    TaskEditComponent,
    FilterPipe,
    OrderByPipe,
    ProjectAddComponent,    
    UserAddComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [TaskService],  
  bootstrap: [AppComponent]
})
export class AppModule { }
