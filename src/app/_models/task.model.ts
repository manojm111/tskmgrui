export default class TaskModel {
    taskId:number;
    parentTaskId:number;
    priority:number;
    startDate:string;
    endDate:string;

}